# Tiny Gigs 

A React.js and Typescript web app for live music in small London venues.
Find tonight's gig [here](http://tinygigs.matttea.com/).

------  

## TODOs

1. Setup example, fake api call as a pattern for SearchResults -> https://yesno.wtf/api

2. Investigate 2 api calls - one to get small venues, and one to get gigs on at those venues (unless they can be in one response from somewhere)  