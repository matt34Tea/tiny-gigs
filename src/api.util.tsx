export const callApi = (
  {
    apiUrl,
    params = {}
  }: ApiProps) => {
  return fetch(apiUrl, {})
    .then(res => {
      if (res.ok) return res.json();
      throw res;
    })
};

interface ApiProps {
  apiUrl: string,
  params: {}
}