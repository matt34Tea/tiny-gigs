import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import userEvent from "@testing-library/user-event";

describe('Search page', () => {
  it('should render find tiny gigs button', () => {
    render(<App />);
    expect(screen.getByRole('button', { name: /find-tiny-gigs/i })).toBeInTheDocument();
    expect(screen.queryByText(/today's tiny gigs/i)).not.toBeInTheDocument();
  });

  it('should return search results', () => {
    render(<App/>);
    userEvent.click(screen.getByRole('button', { name: /find-tiny-gigs/i }));

    expect(screen.getByText(/today's tiny gigs/i)).toBeInTheDocument()
  });
});
