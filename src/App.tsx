import React, { useState } from 'react';
import ButtonBasic from './components/ButtonBasic/ButtonBasic';
import classes from './App.module.css';
import SearchResults from "./components/SearchResults/SearchResults";
import {getGigs} from "./api/gigs";

function App() {
  const [showResults, setShowResults] = useState(false)
  const [results, setResults] = useState({})
  const onClick = async () => {
    setResults(await getGigs())
    setShowResults(true)
  }

  return (
    <div className={`${classes.App}`}>
      <h1>
        For those who like their bands close and their drinks in a glass
      </h1>
      <ButtonBasic
        className={`${classes.buttonBasic}`} // TODO why need a separate className defined here when nothing is being overridden? I think it can only be additional properties, not additional
        ariaLabel="find-tiny-gigs"
        onClick={onClick}
        disabled={false}
      >
        Find tiny gigs
      </ButtonBasic>

      {showResults ?
        <SearchResults
          className={``}
          ariaLabel={`search-results`}
        >
          Today's tiny gigs...
          {results ? <div>{JSON.stringify(results)}</div> : 'no gigs'}
        </SearchResults>
        : null
      }
    </div>
  );
}

export default App;
