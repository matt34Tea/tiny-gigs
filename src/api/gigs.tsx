import {callApi} from "../api.util";
import apiKey from "./apiKey";

export const getGigs = () =>
  callApi({
    apiUrl: `https://app.ticketmaster.com/discovery/v2/events.json?apikey=${apiKey}&countryCode=GB&city=London&size=1`,
    params: {
      method: 'GET',
    }
  });