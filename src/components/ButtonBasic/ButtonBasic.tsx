import React from 'react';
import { Children } from '../../types/common';
import classes from './ButtonBasic.module.css';


function ButtonBasic({ children, ariaLabel = '', onClick, disabled = false }: BasicButtonProps) {
    return (
        <button
            name={'find-tiny-gigs'}
            className={`${classes.buttonBasic}`}
            aria-label={ariaLabel}
            type="button"
            onClick={onClick}
            disabled={disabled}
        >
            {children}
        </button>
    );
}

interface BasicButtonProps extends Children {
    className: string;
    ariaLabel: string;
    onClick: () => void;
    disabled: boolean;
}

export default ButtonBasic;
