import React from 'react';
import { Children } from '../../types/common';
import classes from './SearchResults.module.css';

function SearchResults({ children, ariaLabel = '' }: SearchResultsProps) {
  return (
    <div
      className={`${classes.searchResults}`}
      aria-label={ariaLabel}
    >
      {children}
    </div>
  );
}

interface SearchResultsProps extends Children {
  className: string;
  ariaLabel: string;
}

export default SearchResults;